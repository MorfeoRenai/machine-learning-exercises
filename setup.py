from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='Machine learning exercises and practice. Model fitting, prediction and hyper-parameter tuning. Regression, classification and clustering.',
    author='Alessandro Pandolfi',
    license='MIT',
)
