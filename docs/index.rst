.. machine-learning-exercises documentation master file, created by
   sphinx-quickstart.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

machine-learning-exercises documentation!
==============================================

Contents:

.. toctree::
   :maxdepth: 2

   getting-started
   commands



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
